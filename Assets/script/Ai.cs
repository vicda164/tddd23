﻿using UnityEngine;
using System.Collections;
using System;

public class Ai : MonoBehaviour {


    GameObject[] targetUnits;
    Vector3 target;
    GameObject targetUnit;//is actual targetUnit

    GameObject[] tiles;


    bool isMoving;
    public float speed = 1.5f;

    // Use this for initialization
    void Start () {
        //GetComponent<UnitStats>().mpLeft = 2;
        targetUnit = transform.gameObject; //set own tile as target tile to avoid nullException, importent to replace

       
    }


    // Update is called once per frame
    void Update () {
        if (isMoving)
        {
            //print("AI: isMoving");
            //print("AI: targetTile: " + targetTile);
            transform.position = Vector3.MoveTowards(transform.position, targetUnit.transform.position, (speed * Time.deltaTime));
        }

        if (transform.position == targetUnit.transform.position)
        {
            if (targetUnit.tag == "Unit")
            {
                transform.SetParent(targetUnit.transform.parent); //set tile as parent to unit
            }
            else
            {
                transform.SetParent(targetUnit.transform); //set tile as parent to unit
                if (GetComponent<UnitStats>().mpLeft > 0)
                {
                    makeMove();
                }
            }
            isMoving = false;
            
        }
    }

    internal void makeMove()
    {
        print("makeMove");
        targetUnit = getClosestTargetsRelativPosition();
        print("ai, targetPos: " + targetUnit);
        
        if (targetUnit.tag == "Unit")
        {
            if (gameObject.GetComponent<Combat>().canAttack(targetUnit))
            {
                print("Ai: attack");
                GetComponent<Combat>().fight(transform.gameObject, targetUnit);
            }
            else if (isInFightingRange(targetUnit))
            {
                print("AI: MoveToAttackPos");
                targetUnit = moveToAttackPos(targetUnit);
                isMoving = true;
                

            } else
            {
                print("AI: Move");
                //calculate tile against target
                targetUnit = moveTowards(targetUnit);
                isMoving = true;

            }
        }

    }

    private GameObject moveToAttackPos(GameObject targetUnit)
    {
        //if can't attack closest unit, go toward it!
        tiles = GameObject.FindGameObjectsWithTag("Tile");

        float x = 0f;
        float y = 0f;

        int tempMpLeft = GetComponent<UnitStats>().mp - GetComponent<UnitStats>().fightingRange;//move as little as possible to be able to attack
        Vector3 targetTile = targetUnit.GetComponentInParent<TileStats>().transform.localPosition;
        Vector3 currentTile = transform.GetComponentInParent<TileStats>().transform.localPosition;
     
        while ( (GetComponent<UnitStats>().mpLeft - GetComponent<UnitStats>().fightingRange) > 0)
        {
            x += (targetTile.x < currentTile.x) ? -1f : 1f;

            if (targetTile.x == currentTile.x)
            {
                x = 0f;
            }
            y += (targetTile.y < currentTile.y) ? -1f : 1f;


            //tempMpLeft--;
            GetComponent<UnitStats>().mpLeft -= 1;
        }
       

        print("x: " + x);
        print("y: " + y);
        print("targetTilePost: " + targetTile);
        print("currentTilePost: " + currentTile);


        foreach (GameObject tile in tiles)
        {
            Vector3 tilePos = tile.GetComponent<TileStats>().transform.localPosition;
            if (tilePos.x == currentTile.x + x && tilePos.y == currentTile.y + y)
            {
                print("targetTilePOS: " + tile.transform.position);
                targetUnit = tile;
            }
        }

        return targetUnit;
    }

    private bool isInFightingRange(GameObject targetUnit)
    {
        //when AI can't attack from current tile but have enough MP left to move and attack in same turn.
        int distance = GetComponent<Combat>().getDistanceTo(targetUnit); //Vector3.Distance(gameObject.GetComponentInParent<TileStats>().transform.localPosition,targetUnit.GetComponentInParent<TileStats>().transform.localPosition);
        print("isInFightingRange: " + distance);
        //leser then because it takes 1mp to attack
        if (distance <= gameObject.GetComponent<UnitStats>().mpLeft)
        {
            print("isInFightingRange: true");
            return true;
        }

        return false;
    }
    /*
    private bool canMove(GameObject targetTile)
    {
        print("canMove, targetTile: " + targetTile.transform.parent.position.magnitude);
        print("canMove, targetTile(int): " + (int)targetTile.transform.parent.position.magnitude);
        if (GetComponent<UnitStats>().mpLeft >= (int)targetTile.transform.parent.position.magnitude)
        {
            GetComponent<UnitStats>().mpLeft -= (int)targetTile.transform.parent.position.magnitude;
            return true;
        }

        return false; 
    }
    */
    private GameObject moveTowards(GameObject targetUnit)
    {
        //if can't attack closest unit, go toward it!
        tiles = GameObject.FindGameObjectsWithTag("Tile");

        float x = 0f;
        float y = 0f;

        
        Vector3 targetTile = targetUnit.GetComponentInParent<TileStats>().transform.localPosition;
        Vector3 currentTile = transform.GetComponentInParent<TileStats>().transform.localPosition;

        while (GetComponent<UnitStats>().mpLeft > 0)
        {
            x += (targetTile.x < currentTile.x) ? -1f : 1f;

            if (targetTile.x == currentTile.x)
            {
                x = 0f;
            }
            y += (targetTile.y < currentTile.y) ? -1f : 1f;


            GetComponent<UnitStats>().mpLeft--;
        }
        print("x: " + x);
        print("y: " + y);
        print("targetTilePost: " + targetTile);
        print("currentTilePost: " + currentTile);


        foreach (GameObject tile in tiles)
        {
            Vector3 tilePos = tile.GetComponent<TileStats>().transform.localPosition;
            if (tilePos.x == currentTile.x + x && tilePos.y == currentTile.y + y)
            {
                print("targetTilePOS: " + tile.transform.position);
                targetUnit = tile;
            }
        }

        return targetUnit;
    }

    
    private Vector3 relativPosition(Vector3 target)
    {
        return target - transform.position;
    }

    private bool isEnemyOnTile(GameObject targetTile)
    {
        if (targetTile.transform.childCount > 0)
        {
            if (targetTile.transform.GetChild(0).tag == "Unit")
            {
                return true;
            }
        }
        return false;
    }

    /*
    public bool canMove(Vector3 targetPos)
    {
        print("canMove");
        
        if (GetComponent<UnitStats>().mpLeft >= (int)targetPos.magnitude)
        {
            GetComponent<UnitStats>().mpLeft -= (int)targetPos.magnitude;
            return true;
        }
        
        return false;
    }
    */

    private GameObject getClosestTargetsRelativPosition()
    {
        targetUnits = GameObject.FindGameObjectsWithTag("Unit");
        //target = targetUnits[0].transform.position - transform.position;//relativ distance to target
        targetUnit = targetUnits[0];//saves first tile with unit in
        foreach (GameObject obj in targetUnits)
        {
            if (obj.transform.parent.tag == "Tile")
            {
                
                int dist = GetComponent<Combat>().getDistanceTo(obj);
                //with magnitude it gets easyer to compare and will get AI to chose vertial and horizontal targets before diagonal
                if (dist < GetComponent<Combat>().getDistanceTo(targetUnit))
                {
                    //target = relativPosition(obj.transform.position);
                    targetUnit = obj;//save closest tile object
                }
            }
        
            
        }
        if (!targetUnit.GetComponent<UnitStats>().isActive)
        {
            print("SHOULD NOT HAPPEN!");
            return gameObject;//should not happen but if no enemyfound then stand still
        }

        return targetUnit;
    }
}
