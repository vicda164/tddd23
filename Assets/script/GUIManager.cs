﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class GUIManager : MonoBehaviour {

    public bool isPlayersTurn;
  
    GameObject[] aiUnits;

    private GameObject map;

    Text content;

	// Use this for initialization
	void Start () {
        map = GameObject.FindGameObjectWithTag("Map");


        content = GameObject.FindGameObjectWithTag("BattleLog").GetComponentInChildren<Text>() as Text; 

        Combat.combatMessage += fightMessage;
	}

    private void fightMessage(String msg)
    {
        print("fightMsg: " + msg);
        content.text += msg;
       
    }




    // Update is called once per frame
    void Update () {
	
	}

    public void NextTurn()
    {
        isPlayersTurn = false;
        if (!isPlayersTurn && map.GetComponent<PlayHandler>().turnNr != 0)
        {
            aiUnits = GameObject.FindGameObjectsWithTag("Enemy");
            foreach (GameObject obj in aiUnits)
            {
                obj.GetComponent<UnitStats>().mpLeft = obj.GetComponent<UnitStats>().mp;
                obj.GetComponent<Ai>().makeMove();
            }
        }
        map.GetComponent<PlayHandler>().nextTurn();
        isPlayersTurn = true;
        
    }
}
