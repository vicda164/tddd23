﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


[System.Serializable]
public class Player {

    /*
        This class is for handling and saving the players ingame progress
    */


    public static Player _instance = new Player();

    public string nickname;
    public List<string> unitsOwn;
    public List<string> unitsPlaying;
    public List<string> unitsInChest;
    public int nrOfUnits;

    public int abilityPoints;

    public Player()
    {
        //load in unitsOwn
//        nrOfUnits = unitsOwn.Count;
    }


}
