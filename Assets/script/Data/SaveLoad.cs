﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Linq;

public static class SaveLoad
{   /*
    http://gamedevelopment.tutsplus.com/tutorials/how-to-save-and-load-your-players-progress-in-unity--cms-20934
    */

    public static List<Player> savedGames = new List<Player>();


    //it's static so we can call it from anywhere
    //But everything whanted to be saved need to be specified every time
    public static void Save()
    {
        SaveLoad.savedGames.Add(Player._instance);
        BinaryFormatter bf = new BinaryFormatter();
        //Application.persistentDataPath is a string, so if you wanted you can put that into debug.log if you want to know where save games are located
        FileStream file = File.Create(Application.persistentDataPath + "/savedGames.gd"); //you can call it anything you want
        bf.Serialize(file, SaveLoad.savedGames);
        file.Close();
    }

    public static void Load()
    {
        if (File.Exists(Application.persistentDataPath + "/savedGames.gd"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/savedGames.gd", FileMode.Open);
            SaveLoad.savedGames = (List<Player>)bf.Deserialize(file);
            file.Close();

            Debug.Log("LOAD");
            Debug.Log(Player._instance.nickname);
            //Player._instance.nickname = savedGames.ElementAt(0).nickname;

        }
    }
}
