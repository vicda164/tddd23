﻿using UnityEngine;
using System.Collections;

public class UnitInteraction : MonoBehaviour {

    public Sprite markedSprite;
    public Sprite normalSprite;

    public bool isSelected;

    GameObject[] allPlayersUnits;

	// Use this for initialization
	void Start () {
        isSelected = false;
        allPlayersUnits = GameObject.FindGameObjectsWithTag("Unit");
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnMouseDown()
    {
        print("UnitClick");
        //allPlayersUnits = GameObject.FindGameObjectsWithTag("Unit");
        print(allPlayersUnits.Length);
        if (!isSelected && !isOtherUnitSelected())
        {
            print("selected");
            isSelected = true;
            GetComponent<SpriteRenderer>().sprite = markedSprite;
        }
        else
        {
            isSelected = false;
            GetComponent<SpriteRenderer>().sprite = normalSprite;
        }
    }

    bool isOtherUnitSelected()
    {
        
        foreach (GameObject obj in allPlayersUnits)
        {
            //print(obj.name + obj.GetComponent<UnitInteraction>().isSelected);
            print(obj.name);
            if (obj.GetComponent<UnitInteraction>().isSelected)
            {
                print("is selected");
                return true;
            }

        }
        return false;
    }
}
