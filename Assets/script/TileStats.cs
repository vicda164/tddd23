﻿using UnityEngine;
using System.Collections;

public class TileStats : MonoBehaviour {

    public enum TileType
    {
        BASE,
        ENEMYBASE,
        PLAIN,
        HILL,
        FOREST,
        CHEST
    }

    public TileType tileType;
    public int xCoord;
    public int yCoord;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
