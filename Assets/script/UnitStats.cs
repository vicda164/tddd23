﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class UnitStats : MonoBehaviour {

    public int hp;
    public int ap;
    public int dp;
    public int mp;
    public int mpLeft;
    public UnitType unitType;
    public int fightingRange;

    public bool isDead;
    public bool isVaporized;
    public bool isActive;

    bool isRoundOver;


    public enum UnitType
    {
        MELEE,
        RANGE,
        LEADER
    }
  
    // Use this for initialization
    void Start () {

        
        
        PlayHandler.turnNrUp += resetMpLeft;

        isActive = false;
        //isActive = true;//for debug 
        
	}


    // Update is called once per frame
    void Update () {
        if (isVaporized)
        {
            if (gameObject.tag == "Unit")
            {
                GetComponent<UnitInteraction>().isSelected = false;
            }

            //set anim trigger
            //print("Unit defeated");
            
            Color c = GetComponent<SpriteRenderer>().color; //fade out color of sprite
            c.a -= .05f;
            GetComponent<SpriteRenderer>().color = c;
            //print("alpha: " + c.a);
            //Destroy(gameObject,2f);
            //gameObject.transform.parent.DetachChildren();

            
        }
	}


    private void resetMpLeft()
    {
        mpLeft = mp;
    }

    internal void takeDamage(int ap)
    {
        hp -= ap;

        if (isDead) return;

        if (hp <= 0)
        {
            isDead = true;
            isVaporized = true;

            if (gameObject.tag == "Unit")
            {
                Player._instance.nrOfUnits -= 1;
            }
            
            if (isCurrentPlayingUnitsDead())
            {
                print("isEnemyUnitDefeated");
                GameObject.FindGameObjectWithTag("Map").GetComponent<PlayHandler>().isRoundOver = true;
               
            }
            
        }


       
    }


    private bool isCurrentPlayingUnitsDead()
    {
             
        GameObject[] currentPlayingUnits = GameObject.FindGameObjectsWithTag(gameObject.tag);
        foreach (GameObject obj in currentPlayingUnits)
        {
            if (!obj.GetComponent<UnitStats>().isDead &&
                obj.GetComponent<UnitStats>().isActive)
            { return false; }
        }
        return true;
       
    }

    internal void setActive()
    {
        isActive = true;

        //remove from inChest list
        print("gameObject name: " + gameObject.name);
        int removeFromName = gameObject.name.IndexOf("(");
        Player._instance.unitsInChest.Remove(gameObject.name.Remove(removeFromName));// (clone)

    }

    internal void setInactive()
    {
        if(gameObject.tag == "Unit")
        {
            GetComponent<UnitInteraction>().isSelected = false;
            GetComponent<SpriteRenderer>().sprite = GetComponent<UnitInteraction>().normalSprite;
        }
        isActive = false;
        isDead = true;
    }
}
