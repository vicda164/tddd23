﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

public class CreatePlayerManager : MonoBehaviour {

    Text inputNickname;
    Button createPlayer;

    // Use this for initialization
    void Start()
    {
        
        inputNickname = GameObject.FindObjectOfType<Text>();
        createPlayer = FindObjectOfType<Button>();
        createPlayer.interactable = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!String.IsNullOrEmpty(inputNickname.text))
        {
            createPlayer.interactable = true;
        }
        else
        {
            createPlayer.interactable = false;
        }
       
    }


    public void saveNewPlayer()
    {
        print(inputNickname.name);

        if (!String.IsNullOrEmpty(inputNickname.text))
        {
            Player._instance.nickname = inputNickname.text;
            List<string> l = new List<string>() { "Warrior"};
            Player._instance.unitsOwn = l;
            //Player._instance.unitsOwn.Add("Warrior");
            //Player._instance.unitsOwn.Add("Warrior");
            SaveLoad.Save();
            Application.LoadLevel("Scen1");
        }
    }
    
}
