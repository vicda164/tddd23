﻿using UnityEngine;
using UnityEngine.UI;
using System;

public class MenuManager : MonoBehaviour {


    Button continueButton;

	// Use this for initialization
	void Start () {
        continueButton = GameObject.Find("ContinueButton").GetComponent<Button>();
        continueButton.interactable = false;
        if (SaveLoad.savedGames != null)
        {
            continueButton.interactable = true;
        }
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void createNewPlayer()
    {
        SaveLoad.Load();
        Application.LoadLevel("CreatePlayer");
    }


    public void continueGame()
    {
        Application.LoadLevel("PlayerMenu");
    }

}
