﻿using UnityEngine;
using System.Collections;
using System;

public class MoveManager : MonoBehaviour
{

    public float speed = 1.5f;
    //private Vector3 target;
    Ray ray;
    RaycastHit2D rayHit;
    public bool isMoving;

    GameObject targetTile;
    GameObject targetUnit;

    Combat combatManager;
    UnitStats thisUnitStats;

    Vector3 distanceToTarget;

    GameObject map;

    void Start()
    {
        map = GameObject.FindGameObjectWithTag("Map");
        targetTile = transform.gameObject; //to avoid nullException
        combatManager = GetComponent<Combat>();
        thisUnitStats = GetComponent<UnitStats>();
    }

    void Update()
    {
        if (transform.tag == "Unit")
        {
            if (Input.GetMouseButtonDown(0) && GetComponent<UnitInteraction>().isSelected)
            {
                ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                rayHit = Physics2D.Raycast(ray.origin, ray.direction);
                

                if (rayHit)
                {
                    distanceToTarget = rayHit.collider.transform.position - transform.position;
                    if (!gameObject.GetComponent<UnitStats>().isActive && !gameObject.GetComponent<UnitStats>().isDead)
                    {
                        moveInactiveUnit();                       
                    }   
                    //if tile and the relativ distance is not futher then mpLeft
                    else if (rayHit.collider.tag == "Tile" && canMoveDistance(distanceToTarget))
                    {
                        //if enemy on tile
                        if (rayHit.transform.childCount > 0)
                        {
                            targetUnit = rayHit.transform.GetChild(0).gameObject;
                            if (targetUnit.tag == "Enemy")
                            {
                                print("enemyhit");
                                if (combatManager.canAttack(targetUnit))
                                { 
                                    combatManager.fight(transform.gameObject, targetUnit);
                                }
                                
                                if (targetUnit.GetComponent<UnitStats>().hp <= 0 && thisUnitStats.hp > 0)
                                {
                                    print("hit: " + rayHit.collider.name);
                                    isMoving = true;
                                    targetTile = rayHit.collider.gameObject;
                                    thisUnitStats.mpLeft = 0; //if attacked remove all mpleft
                                }

                            }
                        }
                        else
                        {
                            print("hit: " + rayHit.collider.name);
                            isMoving = true;
                            targetTile = rayHit.collider.gameObject;
                            thisUnitStats.mpLeft -= (int)distanceToTarget.magnitude;
                        }

                    }
                }
            }

            if (isMoving)
            {
                transform.position = Vector3.MoveTowards(transform.position, targetTile.transform.position, (speed * Time.deltaTime));
            }

            if (transform.position == targetTile.transform.position)
            {
                transform.SetParent(targetTile.transform); //set tile as parent to unit
                isMoving = false;
                
            }
        }
    }

    private void moveInactiveUnit()
    {
        if (rayHit.collider.tag == "Tile" && map.GetComponentInParent<PlayHandler>().turnNr == 0)
        {
            print("rayHit: " + rayHit.transform.GetComponent<TileStats>().tileType);
            if (rayHit.transform.GetComponent<TileStats>().tileType == TileStats.TileType.BASE)
            {
                GetComponent<UnitStats>().setActive();
                isMoving = true;
                targetTile = rayHit.collider.gameObject;
            }
        }
    }

    public bool canMoveDistance(Vector3 targetPos)
    {
        print("canMove: " + targetPos.magnitude);
      
        if (GetComponent<UnitStats>().mpLeft >= (int)targetPos.magnitude)
        {
            print("canMove: true");
            //GetComponent<UnitStats>().mpLeft -= (int)targetPos.magnitude;
            return true;
        }
        
        return false;
    }
}

