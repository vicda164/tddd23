﻿using UnityEngine;
using System.Collections;
using System;

public class Combat : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public delegate void CombatMessageHandler(String msg);
    public static event CombatMessageHandler combatMessage;

    internal void fight(GameObject unit, GameObject target)
    {
        //todo: add damage on terraint, opponent pros etc


        UnitStats unitStats = unit.GetComponent<UnitStats>();
        UnitStats targetStats = target.GetComponent<UnitStats>();

        //if target can defend itself then deal damage to attacker(unit)
        if (target.GetComponent<Combat>().canAttack(unit))
        {
            unitStats.takeDamage(targetStats.ap);
        }
        //damage target
        targetStats.takeDamage(unitStats.ap);

        



        if (combatMessage != null)
        {
            combatMessage(unit.gameObject.name + " took " + targetStats.ap + " in damage, have now " + unitStats.hp + " hp ");
            combatMessage(target.gameObject.name + " took " + unitStats.ap + " in damage, have now " + targetStats.hp + " hp ");
        }
     

    }

    internal bool canAttack(GameObject targetTile)
    {
        switch (gameObject.GetComponent<UnitStats>().unitType)
        {
            case UnitStats.UnitType.MELEE:
                return canMeleeAttack(targetTile);
                break;
            case UnitStats.UnitType.RANGE:
                return canRangerAttack(targetTile);
                break;
            case UnitStats.UnitType.LEADER:
                return canLeaderAttack(targetTile);
                break;
            default:
                Debug.LogError("Unvalid unitType");
                return false;
                break; 

        }
    }

    private bool canLeaderAttack(GameObject targetTile)
    {
        throw new NotImplementedException();
    }

    private bool canRangerAttack(GameObject targetUnit)
    {
        UnitStats unitStats = GetComponent<UnitStats>();
        int distance = getDistanceTo(targetUnit);
        if (unitStats.unitType == UnitStats.UnitType.RANGE && Mathf.Abs(distance) == unitStats.fightingRange && unitStats.mpLeft > 0)
        {
            return true;
        }
        return false;

    }

    private bool canMeleeAttack(GameObject targetUnit)
    {
        UnitStats unitStats = GetComponent<UnitStats>();
        int distance = getDistanceTo(targetUnit); 

        print("canMeleeAttack, distance: " + distance);
        print("go pos: " + transform.GetComponentInParent<TileStats>().transform.localPosition);
        print("target pos: " + targetUnit.transform.GetComponentInParent<TileStats>().transform.localPosition);
        //melee units need to stand on a tile distance of 1 to its target
        if (Mathf.Abs(distance) == 1 && unitStats.mpLeft > 0)
        {
            print("AI: can attack!");
            return true;
        }
        print("AI: can not attack");
        return false;
    }

    public int getDistanceTo(GameObject targetUnit)
    {
        print(targetUnit.transform.position);
        return (int)Vector3.Distance(transform.GetComponentInParent<TileStats>().transform.localPosition,
            targetUnit.transform.GetComponentInParent<TileStats>().transform.localPosition);

    }
}
