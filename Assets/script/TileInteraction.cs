﻿using UnityEngine;
using System.Collections;

public class TileInteraction : MonoBehaviour {

    public Sprite normalSprite;
    public Sprite focusSprite;


    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }

    void OnMouseEnter()
    {
        GetComponent<SpriteRenderer>().sprite = focusSprite;
    }

    void OnMouseExit()
    {
        GetComponent<SpriteRenderer>().sprite = normalSprite;
    }

    void OnMouseDown()
    {
        print("CLICK");
        print("tile click:" + this.transform.position);
        if (transform.childCount > 0)
        {
            if (transform.GetChild(0).tag == "Unit")
            {
                transform.GetChild(0).gameObject.GetComponent<UnitInteraction>();
            }
        }
    }
}
