﻿using UnityEngine;
using System.Collections;
using System;
using System.Linq;
using System.Collections.Generic;

public class PlayHandler : MonoBehaviour {

    public int roundNr;
    public bool isRoundOver;

    public int turnNr;

    public int BESTOF = 2; //best of 3 rounds

    public int playerScore;
    public int enemyScore;

    public GameObject aiUnit;

    GameObject[] enemyUnits;
    GameObject[] playerUnits;

    private int NR_OF_AI = 2;

    /*
       Both parts starts with score = 2; For each lose, one point is drawn.
    */

	// Use this for initialization
	void Start () {
        //creates user for debugng
        Player._instance.nickname = "Victor";
        List<string> l = new List<string>() { "Warrior", "Warrior", "Ranger"};
        Player._instance.unitsOwn = l;
        Player._instance.unitsPlaying = l;
        Player._instance.unitsInChest = l;
        SaveLoad.Save();//for debuging
        SaveLoad.Load();



        playerScore = 2;
        enemyScore = 2;
        

	}
	
	// Update is called once per frame
	void Update () {
        if (isRoundOver)
        {
            print("isRoundOver");
            //give points
            
            playerUnits = GameObject.FindGameObjectsWithTag("Unit");
            enemyUnits = GameObject.FindGameObjectsWithTag("Enemy");
            if (isAllUnitsDead(playerUnits) && isAllUnitsDead(enemyUnits))
            {
                print("EVEN");
                setScore("even");
            }
            else if (isAllUnitsDead(playerUnits))
            {
                print("ENEMY WON");
                //all playerUnits is dead but NOT all enemys
                setScore("Enemy");
            }
            else if (isAllUnitsDead(enemyUnits))
            {
                print("PLAYER WON");
                setScore("Unit");
            }

            //setScore(gameObject.tag.ToString());


            //start new round or if this was last round then end game
            nextRound();

            isRoundOver = false;
        }
    }

    private bool isAllUnitsDead(GameObject[] units)
    {
        foreach (GameObject unit in units)
        {
            UnitStats unitStats = unit.GetComponent<UnitStats>();
            if (unitStats.isActive && !unitStats.isDead)
            {
                return false;
            }

        }
        isRoundOver = true;
        return true;
    }

    public delegate void TurnNrGrow();
    public static event TurnNrGrow turnNrUp;

    public void nextTurn()
    {
        if (turnNr == 0)
        {
            createAi();
        }
        turnNr++;
        if (turnNrUp != null)
        {
            turnNrUp();
        }
    }

    private void createAi()
    {
        IEnumerable<GameObject> enemyBase = new List<GameObject>( GameObject.FindGameObjectsWithTag("Tile")).Where(tile =>
                                                    tile.GetComponent<TileStats>().tileType == TileStats.TileType.ENEMYBASE);
        for (int index = 0; index < NR_OF_AI; index++)
        {

            GameObject g = Instantiate(aiUnit,
              enemyBase.ElementAt(index).transform.position, Quaternion.identity) as GameObject;

            g.GetComponent<UnitStats>().mpLeft = g.GetComponent<UnitStats>().mp;

            //g.transform.Translate(g.transform.parent.position);
            
            g.transform.SetParent(gameObject.transform.GetChild(index).transform);

            g.transform.Translate(g.transform.parent.position - g.transform.position);
        }

    }

    public delegate void RoundNrGrow();
    public static event RoundNrGrow roundNrUp;

    public void nextRound()
    {
        if (roundNr <= BESTOF && playerScore > 0 && enemyScore > 0)
        {
            roundNr++;
            //reset turnNr
            turnNr = 0;
            
            clearMap();
            //fillChest
            //GameObject chest = GameObject.FindGameObjectWithTag("Chest");
            //chest.GetComponent<ChestManager>().fillChest();


            if (roundNrUp != null)
            {
                roundNrUp();
            }
        }
        else if (playerScore == 0)
        {
            print("Enemy won game");
        }
        else if (enemyScore == 0)
        {
            print("Player won game");
            
        }
    }

    private void clearMap()
    {
        List<GameObject> units = new List<GameObject>(GameObject.FindGameObjectsWithTag("Unit"));
        List<GameObject> enemys = new List<GameObject>(GameObject.FindGameObjectsWithTag("Enemy"));
        foreach (GameObject unit in units)
        {
            print("UNIT");
            if (unit.GetComponent<UnitStats>().isActive)
            {
                print("DESTROY: " + unit.GetComponentInParent<TileStats>().transform.localPosition);

                moveToGrave(unit);
                
            }
        }

        enemys.ForEach(enemy => moveToGrave(enemy));
        enemys.ForEach(enemy => Destroy(enemy,1f));
    }

    private void moveToGrave(GameObject unit)
    {
        //Destroy(unit);
        //unit.GetComponent<UnitStats>().takeDamage(1000);
        GameObject b = GameObject.Find("Box").gameObject;
        unit.transform.Translate(b.transform.position - unit.transform.position);
        unit.transform.SetParent(b.transform);
        unit.GetComponent<UnitStats>().setInactive();
    }

   

    internal void setScore(string winner)
    {
        //tag is the winner
        if (winner == "Unit")
        {
            enemyScore -= 1;
        }
        else if (winner == "Enemy")
        {
            playerScore -= 1;
        }
        else
        {
            //even
            enemyScore -= 1;
            playerScore -= 1;
        }
    }
}
