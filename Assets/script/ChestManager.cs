﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class ChestManager : MonoBehaviour {


    public GameObject warrior;
    public GameObject ranger;

    GameObject[] chestTiles;
    private int units =4;

    
	// Use this for initialization
	void Start () {

        fillChest();//fill chest with player units
	    
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void fillChest()
    {
        GameObject unit;
        string unitName;

        for (int index = 0; index < Player._instance.unitsInChest.Count; index++)
        {
            unitName = Player._instance.unitsInChest.ElementAt(index);
            print("resources load: " + unitName);
            unit = Resources.Load(unitName, typeof(GameObject)) as GameObject; //GameObject.Find(unitName).gameObject;
            GameObject g = Instantiate(unit,
            gameObject.transform.GetChild(index).transform.position, Quaternion.identity) as GameObject;

            g.transform.SetParent(gameObject.transform.GetChild(index).transform);
        }
        
    }
}
